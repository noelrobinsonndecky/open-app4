const Fruits = [
    {
        nom: "Banane",
        couleur: "Jaune",
        Poids: "300g",
        Prix: "200 Fcfa"
    },
    {
        nom: "Pomme",
        couleur: "vert",
        Poids: "200g",
        Prix: "100 Fcfa"
    },
    {
        nom: "0range",
        couleur: "orange",
        Poids: "50g",
        Prix: "50 Fcfa"
    },
    {
        nom: "Citron",
        couleur: "Jaune",
        Poids: "20g",
        Prix: "25 Fcfa"
    }
]

const Liste2 = () => {
    return (
        <ul>
            {Fruits.map(({nom,couleur, Poids, Prix}) => (
                <li key={nom}>
                    {[
                        "Fruit: " + nom + "  " + 
                        "Couleur: " + couleur + "  " + 
                        "Poids: " + Poids + "  " + 
                        "Prix: " + Prix
                    ]}
                </li>
            ))}
        </ul>
    );
};

export default Liste2;