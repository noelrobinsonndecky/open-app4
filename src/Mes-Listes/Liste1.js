const listeA = [
    "Jean",
    "Noel",
    "Moussa",
    "Bamba",
    "Mané",
    "Fati",
    "Nafi"
]

const L1 = () => {

    return (
        <ul>
            {listeA.map((item, index) => (
                <li key={index}>{item}</li>
            ))}
        </ul>
    )
}

export default L1;