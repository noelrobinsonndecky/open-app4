const personnes = [
    {
        id: 1,
        nom: "Fatou",
        prénom: "Diouf",
        adresse: "Dakar",
        Tel: "77 354 36 38",
        Age: 25,
        majeur: true
    },
    {
        id: 2,
        nom: "Dior",
        prénom: "Gueye",
        adresse: "Rufisque",
        Tel: "77 354 36 38",
        Age: 15,
        majeur: false
    },
    {
        id: 3,
        nom: "Ismael",
        prénom: "Sene",
        adresse: "Bargny",
        Tel: "77 354 36 38",
        Age: 24,
        majeur: true
    },
    {
        id: 4,
        nom: "Sali",
        prénom: "Ndoye",
        adresse: "Louga",
        Tel: "77 354 36 38",
        Age: 19,
        majeur: true
    },
    {
        id: 5,
        nom: "Jean",
        prénom: "Mendy",
        adresse: "Matam",
        Tel: "77 354 36 38",
        Age: 17,
        majeur: false
    }
]

const Liste4 = () => {
    return (
        <ul>
            {personnes.map(({id,nom, prénom, majeur}) => (
                <li key={id}>
                    {
                        majeur? 
                            [
                                "Identité " , " " , id , ": " ,
                                "Nom: " , " " , nom , " " ,
                                "Prenom: " , " " , prénom , " " ,
                                "Majeur: " , " " , majeur
                            ] 
                            
                            : 
                            null 
                    }
                </li>
            ))}
        </ul>
    );
};

export default Liste4;