function MaFonction(props){

    const valeurFonction = props.maFonction;

    const mesDonnées = [1, 2, 3, 4];
    return (
        <div>
            {mesDonnées.map((data) => (
                valeurFonction >= data ? <span key={data.toString()}></span>: null
            ))}
        </div>
    )
}

export default MaFonction;